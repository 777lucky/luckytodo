const chai = require('chai')
const chaiHttp = require('chai-http')
const {
    expect
} = chai;

chai.use(require('chai-http'));
const server = require('../index.js')

let User = require('../models/user.js')
let Task = require('../models/task.js')

// const userFixture = require('../fixtures/userFixture.js')
// const userSample = userFixture.create
// const taskFixture = require('../fixtures/taskFixtures.js')
// const taskSampe = taskFixture.create

describe('Task API Unit Testing', () => {
    before(function() {
        //User.deleteMany({})
        User.deleteMany({}).then();
        // User.register({
        //     name: 'test',
        //     email: 'unittesting@mail.com',
        //     password: '123123',
        //     password_confirmation: '123123'
        // })
    })

    after(function() {
        User.deleteMany({});
        //Task.deleteMany({});
        //User.deleteMany({}).then(i => done());
    })

    //Testing register API
    context('POST /api/v1/users/register', () => {
        //positive
        it('Should create new user', function() {
            let user = {
                name: 'testing',
                email: 'unittesting@mail.com',
                password: '123456',
                password_confirmation: '123456'
            }

            chai.request(server)
            .post('/api/v1/users/register')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                console.log(res.body)
                expect(res.status).to.eq(201)
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.property('status')
                expect(res.body).to.have.property('data')
            })
        })
        //negative
        it('Should have failed to create new user', function() {
            let user = {
                name: 'testing',
                email: 'unittesting@mail.com',
                password: '123456',
                password_confirmation: '123456'
            }

            chai.request(server)
            .post('/api/v1/users/register')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                console.log(res.body)
                expect(res.status).to.eq(422)
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.property('status')
                //expect(res.body).to.have.property('data')
            })
        })
    })


    //Testing Login API
    context('POST /api/v1/users/login', () => {
        it('Should logged in', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }

            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                console.log(err);
                console.log(res.body);
                expect(res.status).to.eq(200);
            })
        })
        //negative
        it('Should have not logged in', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '1234567'
            }

            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                console.log(err);
                console.log(res.body);
                expect(res.status).to.eq(400);
            })
        })
    })

    //Testing Create Task
    context('POST /api/v1/tasks/create', () => {
        it('Should create a new task for an authorized user', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            let task = {
                name: 'test',
                description: 'unit testing by chai and mocha',
                due_date: '2020-02-21'
            }

            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .post('/api/v1/tasks/create')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .send(JSON.stringify(task))
                .end(function(err, res){
                    console.log(res.body)
                    expect(res.status).to.equal(201);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                    expect(data).to.be.an('object');
                })
            })
        })
    })

    context('GET /api/v1/tasks/show', () => {
        it('Should show task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                    
                })
            })
        })
    })

    context('GET /api/v1/tasks/show/uncomplete', () => {
        it('Should show uncomplete task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show/uncomplete')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    console.log(res.body)
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                })
            })
        })
    })

    context('GET /api/v1/tasks/show/completed', () => {
        it('Should show completed task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show/completed')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                })
            })
        })
    })

    context('GET /api/v1/tasks/show/unimportance', () => {
        it('Should show unimportance task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show/unimportance')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                })
            })
        })
    })

    context('GET /api/v1/tasks/show/importance', () => {
        it('Should show importance task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show/importance')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                })
            })
        })
    })
    
    /*
    context('PUT /api/v1/tasks/update/:_id', () => {
        it('Should update task by task id', function(){
            let user = {
                email: 'unittesting@mail.com',
                password: '123456' 
            }
            let update = {
                importance: true,
                completion: true
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {

            })
        })
    })

    context('DELETE /api/v1/tasks/delete/:_id', () => {
        it('Should delete task by user id', function() {
            let user = {
                email: 'unittesting@mail.com',
                password: '123456'
            }
            chai.request(server)
            .post('/api/v1/users/login')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                chai.request(server)
                .get('/api/v1/tasks/show')
                .set('Content-Type', 'application/json')
                .set('Authorization', res.body.data.token)
                .end(function(err, res){
                    expect(res.status).to.equal(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body).to.have.property('data');
                    let {success, data} = res.body;
                    
                })
            })
        })
    })
    */
})